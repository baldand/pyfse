from distutils.core import setup, Extension

module1 = Extension('pyfse', sources = ["pyfse.c","fse.c"],  extra_compile_args=["-O3"], extra_link_args=[])

setup ( name = "pyfse", version = 1.0, description = "Python wrapper for Finite State Entropy compressor", ext_modules = [module1])


