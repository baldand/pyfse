#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include "fse.h"

static PyObject*
pyfse_compress(PyObject* self, PyObject *args)
{
    const char* input = 0;
    Py_ssize_t length = 0;
    if (!PyArg_ParseTuple(args, "s#", &input, &length))
        return NULL;
    Py_ssize_t maxlength = FSE_compressBound(length);
    PyObject* ba = PyString_FromStringAndSize(NULL,maxlength);
    char* baptr = (char*)PyString_AsString(ba);
    if (baptr == NULL) {
        Py_DECREF(ba);
        return NULL;
    }
    int size = FSE_compress(baptr,input,length);
    if (_PyString_Resize(&ba,size)==-1)
        return NULL;
    return ba;
}

static PyObject*
pyfse_decompress(PyObject* self, PyObject *args)
{
    const char* input = 0;
    Py_ssize_t length = 0;
    Py_ssize_t original = 0;
    if (!PyArg_ParseTuple(args, "s#n", &input, &length, &original))
        return NULL;
    PyObject* ba = PyString_FromStringAndSize(NULL,original);
    char* baptr = (char*)PyString_AsString(ba);
    if (baptr == NULL) {
        Py_DECREF(ba);
        return NULL;
    }
    int size = FSE_decompress(baptr,original,input);
    printf("size=%d\n",size);
    return ba;
}

static PyMethodDef methods[] = {
    { "compress", pyfse_compress, METH_VARARGS, "Compress a string using Finite State Entropy encoder" },
    { "decompress", pyfse_decompress, METH_VARARGS, "Decompress a string encoded using Finite State Entropy encoder" },
    { NULL, NULL, 0, NULL }
};

PyMODINIT_FUNC
initpyfse(void)
{
    PyObject* m;

    m = Py_InitModule("pyfse", methods);
    if (m == NULL)
        return;
    PyModule_AddStringConstant(m,"__version__","1.0");
}

