#!/usr/bin/python

import pyfse,time

def test(slong):
    print "Test string length:",len(slong)
    start = time.time()
    cs = pyfse.compress(slong)
    end = time.time()
    print "Compressed string length:",len(cs)
    print "Compression took",end-start,"seconds",100.0*float(len(cs))/float(len(slong)),"%",float(len(slong))/(end-start),"bytes/second"
    start = time.time()
    dcs = pyfse.decompress(cs,len(slong))
    end = time.time()
    if len(dcs)!=len(slong):
        print "Length different!"
    if dcs != slong:
        print "Content different!"
    else:
        print "Content same."
    print "Decompression took",end-start,"seconds",float(len(slong))/(end-start),"bytes/second"
    print len(dcs)

s = "I am a test of a normal english string. The quick brown fox jumped over the lazy dog. That probably doesn't compress so well, but spaces will at least! Here's a long bunch of eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee."*350000

test(s)

s = "\0\0\1\0\0\2"*16500000

test(s)

s = ("\0\0\0\0\0\0"*100+"\1")*165000

test(s)




